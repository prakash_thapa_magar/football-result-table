import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { baseUrl } from "../config";

export const footballScoresApi = createApi({
  reducerPath: "footballScoresApi",
  baseQuery: fetchBaseQuery({
    baseUrl,
  }),
  tagTypes: ["readFootballScoresDetail"],
  endpoints: (builder) => ({
    readFootballScores: builder.query({
      query: (query) => `${baseUrl}${query}`,
    }),

    providesTags: ["readFootballScoresDetail"],
  }),
});

export const { useReadFootballScoresQuery } = footballScoresApi;
