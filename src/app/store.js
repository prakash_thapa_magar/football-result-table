import { configureStore } from "@reduxjs/toolkit";
import { setupListeners } from "@reduxjs/toolkit/query";
import { footballScoresApi } from "../services/footballScores";

export const store = configureStore({
  reducer: {
    [footballScoresApi.reducerPath]: footballScoresApi.reducer,
  },

  middleware: (getDefaultMiddleware) =>
    getDefaultMiddleware().concat(footballScoresApi.middleware),
});

setupListeners(store.dispatch);
