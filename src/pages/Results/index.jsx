import { useState, useEffect, useMemo, useCallback } from "react";
import Table from "../../Components/ReactTable";
import DetailsPage from "../../Container/DetailsPage";
import Modal from "react-modal";
import {
  ArrowDownIcon,
  ArrowUpIcon,
  DotsCircleHorizontalIcon,
} from "@heroicons/react/outline";
import { useReadFootballScoresQuery } from "../../services/footballScores";

const FootballResultTable = () => {
  const [openModal, setOpenModal] = useState(false);
  const [details, setDetails] = useState({});

  const [matches, setMatches] = useState();

  const closeModal = () => {
    setOpenModal(false);
  };
  const handleOpenModal = () => {
    setOpenModal(true);
  };

  const {
    data: footballData,
    error: isFetchError,
    isLoading,
  } = useReadFootballScoresQuery("");

  useEffect(() => {
    setMatches(footballData?.matches);
  }, [footballData]);

  //filter Club Names from an array

  const getAllClubs = useCallback(() => {
    const teams = matches?.flatMap((match) => {
      const { team1, team2 } = match;
      return [team1, team2];
    });
    const uniqueTeams = [...new Set(teams)];
    return uniqueTeams;
  }, [matches]);

  const getFromatedData = useCallback(() => {
    const clubsData = {};
    const clubs = getAllClubs();

    matches?.forEach(({ team1, team2, date, round, score }) => {
      clubs.forEach((club) => {
        clubsData[club] = clubsData[club] || {};
        if ((club === team1 || club === team2) && score) {
          // played
          let played = clubsData[club].played ?? 0;
          played++;

          // won/lose
          let won = clubsData[club].won ?? 0;
          let lost = clubsData[club].lost ?? 0;
          let draw = clubsData[club].draw ?? 0;
          let gf = clubsData[club].gf ?? 0;
          let ga = clubsData[club].ga ?? 0;
          let status = "";

          const team1Score = score?.ft?.[0];
          const team2Score = score?.ft?.[1];

          if (club === team1) {
            gf = gf + team1Score;
            ga = ga + team2Score;
            if (team1Score > team2Score) {
              won++;
              status = "W";
            } else if (team2Score > team1Score) {
              lost++;
              status = "L";
            } else if (team1Score === team2Score) {
              draw++;
              status = "D";
            }
          }

          if (club === team2) {
            gf = gf + team2Score;
            ga = ga + team1Score;
            if (team2Score > team1Score) {
              won++;
              status = "W";
            } else if (team1Score > team2Score) {
              lost++;
              status = "L";
            } else if (team1Score === team2Score) {
              draw++;
              status = "D";
            }
          }
          clubsData[club].played = played;
          clubsData[club].won = won;
          clubsData[club].lost = lost;
          clubsData[club].draw = draw;
          clubsData[club].gf = gf;
          clubsData[club].ga = ga;
          clubsData[club].gd = gf - ga;
          clubsData[club].position = 1;
          clubsData[club].points = won * 3 + draw * 1;

          let allGames = clubsData[club].allGames ?? [];
          allGames.push({ team1, team2, date, round, score, status });
          allGames = allGames?.sort((a, b) => new Date(a) - new Date(b));
          allGames = allGames?.slice(-5);
          clubsData[club].allGames = allGames;
        }
      });
    });

    const finalClubsData = Object.entries(clubsData)
      .map(([clubName, clubData]) => ({
        clubName,
        ...clubData,
      }))
      .sort((a, b) => b?.points - a?.points);
    return finalClubsData;
  }, [matches]);

  const images = [
    {
      name: "Manchester City FC",
      logo: "/images/manchester.png",
    },
    {
      name: "Manchester United FC",
      logo: "/images/manchesterUnited.png",
    },

    {
      name: "Liverpool FC",
      logo: "/images/liverpool.png",
    },
    {
      name: "Chelsea FC",
      logo: "/images/chelsea.png",
    },
    {
      name: "Leicester City FC",
      logo: "/images/leicesterCity.png",
    },
    {
      name: "West Ham United FC",
      logo: "/images/westHamUnited.png",
    },
    {
      name: "Tottenham Hotspur FC",
      logo: "/images/tottenhamHotspur.png",
    },

    {
      name: "Arsenal FC",
      logo: "/images/arsenal.png",
    },

    {
      name: "Leeds United FC",
      logo: "/images/leedsUnited.png",
    },
    {
      name: "Everton FC",
      logo: "/images/everton.png",
    },
    {
      name: "Aston Villa FC",
      logo: "/images/astonVilla.png",
    },
    {
      name: "Newcastle United FC",
      logo: "/images/newcastleunited.png",
    },

    {
      name: "Wolverhampton Wanderers FC",
      logo: "/images/wolverhampton.png",
    },

    {
      name: "Crystal Palace FC",
      logo: "/images/crystalPalace.png",
    },
    {
      name: "Southampton FC",
      logo: "/images/southampton.png",
    },
    {
      name: "Brighton & Hove Albion FC",
      logo: "/images/brighton.png",
    },

    {
      name: "Burnley FC",
      logo: "/images/burnley.png",
    },

    {
      name: "Fulham FC",
      logo: "/images/fulham.png",
    },
    {
      name: "West Bromwich Albion FC",
      logo: "/images/westbromwich.png",
    },

    {
      name: "Sheffield United FC",
      logo: "/images/shefieldunited.png",
    },
  ];

  const clubValues = getFromatedData();
  const clubsData = clubValues.map((obj, index) => ({
    ...obj,
    sn: index + 1,
    logo: images
      .filter((resData) => resData.name === obj.clubName)
      .map((resMap) => resMap.logo),
  }));

  const columns = useMemo(
    () => [
      {
        Header: "Position",
        accessor: "position",

        Cell: ({ row: { original }, cell: { value } }) => {
          console.log("original", original);
          return (
            <div className="icon-holder">
              <div>{original.sn}</div>

              <>
                {original?.allGames?.[3]?.status === "W" &&
                original?.allGames?.[4]?.status === "W" ? (
                  <div>
                    <ArrowUpIcon className="arrow-up" />
                  </div>
                ) : original?.allGames?.[3]?.status === "W" &&
                  original?.allGames?.[4]?.status === "L" ? (
                  <ArrowDownIcon className="arrow-down" />
                ) : (
                  <DotsCircleHorizontalIcon className="arrow-dot" />
                )}
              </>
            </div>
          );
        },
      },
      {
        Header: "club",
        accessor: "clubName",

        Cell: ({ row: { original }, cell: { value } }) => {
          return (
            <>
              <div className="columnContent">
                <div className="image-holder">
                  <img
                    src={original?.logo[0]}
                    alt="images"
                    height={20}
                    width={20}
                  />
                </div>
                <p
                  onClick={() => {
                    handleOpenModal();
                    setDetails(original);
                  }}
                  className="clickModel"
                >
                  {original?.clubName}
                </p>
              </div>
            </>
          );
        },
      },
      {
        Header: "Played",
        accessor: "played",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "Won",
        accessor: "won",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "Drawn",
        accessor: "draw",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "Lost",
        accessor: "lost",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "GF",
        accessor: "gf",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "GA",
        accessor: "ga",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "GD",
        accessor: "gd",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "Points",
        accessor: "points",
        Cell: ({ cell: { value } }) => value || "-",
      },
      {
        Header: "Form",
        accessor: "form",

        Cell: ({ row: { original }, cell: { value } }) => {
          return (
            <>
              <div className="columnContent">
                {original?.allGames?.map((tag) => {
                  return (
                    <>
                      <div
                        className={`tag ${
                          tag.status === "W"
                            ? "win-tag"
                            : tag.status === "L"
                            ? "loss-tag"
                            : "draw-tag"
                        } `}
                      >
                        {tag.status}
                      </div>
                    </>
                  );
                })}
              </div>
            </>
          );
        },
      },
    ],
    []
  );

  return (
    <div className="container">
      <Modal
        isOpen={openModal}
        className="my-modal order"
        overlayClassName="modal-overlay"
      >
        <button
          type="button"
          onClick={() => {
            closeModal();
          }}
          className="close-button"
        >
          <svg
            xmlns="http://www.w3.org/2000/svg"
            className="svgimage"
            viewBox="0 0 20 20"
            fill="currentColor"
          >
            <path
              fillRule="evenodd"
              d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z"
              clipRule="evenodd"
            />
          </svg>
        </button>

        <div className="modal-content">
          <DetailsPage details={details} />
        </div>
      </Modal>
      <div>
        <Table
          data={clubsData}
          columns={columns}
          totalData={clubsData?.length}
          isLoadingData={isLoading}
          isFetchError={isFetchError}
        />
      </div>
    </div>
  );
};

export default FootballResultTable;
