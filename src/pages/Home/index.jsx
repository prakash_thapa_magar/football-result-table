import { useReadFootballScoresQuery } from "../../services/footballScores";

const Home = () => {
  const { data, error, isLoading } = useReadFootballScoresQuery("");
  return (
    <section>
      <div className="container ">
        {error ? (
          <>Oh no, there was an error</>
        ) : isLoading ? (
          <>Loading...</>
        ) : data ? (
          <>
            <h1 className="h1">{data?.name}</h1>
            <div className="imageholder">
              <img src="/images/cover.jfif" alt="premierLeague" />
            </div>
          </>
        ) : null}
      </div>
    </section>
  );
};

export default Home;
