import React from "react";

const DetailsPage = ({ details }) => {
  console.log("details", details);
  return (
    <div className="container">
      <div>
        <div>
          <img src={details?.logo?.[0]} alt="logoImage" width="40px" />
        </div>

        <p>
          Club Name:&nbsp;
          <span className="h1">{details?.clubName}</span>
        </p>

        <div className="columnContent">
          {details?.allGames?.map((tag) => {
            return (
              <>
                <div
                  className={`tag ${
                    tag.status === "W"
                      ? "win-tag"
                      : tag.status === "L"
                      ? "loss-tag"
                      : "draw-tag"
                  } `}
                >
                  {tag.status}
                </div>
              </>
            );
          })}
        </div>

        <div className="club-Details">
          <p>
            Game Played:&nbsp;
            <span className="h1">{details?.played}</span>
          </p>
          <p>
            Won:&nbsp;
            <span className="h1">{details?.won}</span>
          </p>
        </div>
        <div className="club-Details">
          {" "}
          <p>
            Lost:&nbsp;
            <span className="h1"> {details?.lost}</span>
          </p>
          <p>
            Draw:&nbsp;
            <span className="h1">{details?.draw}</span>
          </p>
        </div>

        <div className="club-Details">
          {" "}
          <p>
            GF:&nbsp;
            <span className="h1"> {details?.gf}</span>
          </p>
          <p>
            GA:&nbsp;
            <span className="h1">{details?.ga}</span>
          </p>
        </div>
        <div className="club-Details">
          <p>
            GD:&nbsp;
            <span className="h1"> {details?.gd}</span>
          </p>
          <p>
            Points:&nbsp;
            <span className="h1">{details?.points}</span>
          </p>
        </div>
      </div>
    </div>
  );
};

export default DetailsPage;
