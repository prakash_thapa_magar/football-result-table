import { useTable } from "react-table";
import PropTypes from "prop-types";
function Table({
  columns,
  data,
  showTableFoot = true,
  totalData,
  isLoadingData,
  isFetchError,
}) {
  const { getTableProps, getTableBodyProps, headerGroups, rows, prepareRow } =
    useTable({ columns, data });

  return (
    <>
      <table {...getTableProps()} className="table">
        <thead className="thead">
          {headerGroups.map((headerGroup) => (
            <tr {...headerGroup.getHeaderGroupProps()} className="trHead">
              {headerGroup.headers.map((column) => (
                <th {...column.getHeaderProps()} className="th">
                  {column.render("Header")}
                </th>
              ))}
            </tr>
          ))}
        </thead>
        <tbody {...getTableBodyProps()} className="tbody">
          {isFetchError ? (
            <tr>
              <td className="errordata">Whoops, Error loading data</td>
            </tr>
          ) : isLoadingData ? (
            <tr></tr>
          ) : data && data.length ? (
            <>
              {rows.map((row) => {
                prepareRow(row);
                return (
                  <tr {...row.getRowProps()} className="trBody">
                    {row.cells.map((cell) => {
                      return (
                        <td {...cell.getCellProps()} className="td">
                          {cell.render("Cell")}
                        </td>
                      );
                    })}
                  </tr>
                );
              })}
            </>
          ) : (
            <tr>
              <td className="errordata">Sorry, no data found.</td>
            </tr>
          )}
        </tbody>
      </table>
      {showTableFoot && (
        <div className="tableFooter">
          <p className="p">
            <span className="text-bold">Showing Data:</span> {totalData} out of{" "}
            {totalData}
          </p>
          <p className="p">
            <span className="text-bold">Total Data:{totalData}</span>
          </p>
        </div>
      )}
    </>
  );
}

Table.propTypes = {
  columns: PropTypes.arrayOf(PropTypes.object).isRequired,
  data: PropTypes.arrayOf(PropTypes.object).isRequired,
  isFetchError: PropTypes.bool,
  isLoadingData: PropTypes.bool.isRequired,
};

export default Table;
